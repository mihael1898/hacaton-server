var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require("body-parser");
var cors = require("cors");
// var expressSession = require('express-session');
var cookieSession = require("cookie-session");
var passport = require('passport');
require('./passport');
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors())
app.use(express.static('public'));



// var corsOptions = {
//   origin: "http://localhost:3000"
// };

// app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(expressSession({ 
//   secret: 'be5394b64d14794932fe1a1915a379f3', 
//   resave: true, 
//   saveUninitialized: true 
// }))
// app.use(expressSession({ 
//   secret: 'be5394b64d14794932fe1a1915a379f3', 
//   resave: true,
//   rolling: true,
//   saveUninitialized: false,
//   cookie: {
//     maxAge: 5 * 60 * 1000,
//     httpOnly: false,
//   }, 
// }));
app.use(
  cookieSession({
    name: "session",
    httpOnly: false,
    keys: ['be5394b64d14794932fe1a1915a379f3'],
    maxAge: 24 * 60 * 60 * 1000
  })
);
app.use(passport.initialize());
app.use(passport.session());

const db = require("./app/models");

db.sequelize.sync();
// // drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bezkoder application." });
});

require("./app/routes/auth.routes")(app);
require("./app/routes/role.routes")(app);
require("./app/routes/user.routes")(app);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
