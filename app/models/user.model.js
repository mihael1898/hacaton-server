const fs = require("fs")
module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    name: {
      type: Sequelize.STRING
    },
    pineToolId: {
      type: Sequelize.STRING
    },
    surname: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    position: {
      type: Sequelize.STRING
    },
    about: {
      type: Sequelize.STRING
    },
    url_img: {
      type: Sequelize.STRING
    },
    url_img_for_delete: {
      type: Sequelize.STRING
    }
  }, {
    hooks: {
      afterUpdate: (user, options) => {
        if (user._changed.url_img && user._previousDataValues.url_img_for_delete) {
          fs.unlink(user._previousDataValues.url_img_for_delete, function(err){
            if (err) {
              console.log(err);
            } else {
              console.log("Файл удалён");
            }
          });
        }
      }
    }
  }
);

  return User;
};
