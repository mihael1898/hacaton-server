module.exports = (sequelize, Sequelize) => {
    const Zoom_meeting = sequelize.define("zoom_meeting", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      idZoom: {
        type: Sequelize.STRING
      },
      startUrl: {
        type: Sequelize.STRING
      },
      joinUrl: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      }
    });
  
    return Zoom_meeting;
  };
  