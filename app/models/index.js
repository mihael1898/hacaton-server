const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

var Roles = require("./role.model.js")(sequelize, Sequelize);
var Users = require("./user.model.js")(sequelize, Sequelize);
var Users_like = require("./users_like.model.js")(sequelize, Sequelize);
var Zoom_meeting = require("./zoom_meeting.model.js")(sequelize, Sequelize);

// 1 to 1
Users.belongsTo(Roles, {as: 'role', foreignKey: 'user_role'});

// m to m
Users.belongsToMany(Users, { as: 'children', foreignKey: 'parentUserId', through: Users_like });
Users.belongsToMany(Users, { as: 'parent', foreignKey: 'childrenUserId', through: Users_like });

Users.belongsToMany(Users, { as: 'childrenZoom', foreignKey: 'parentUserId', through: Zoom_meeting });
Users.belongsToMany(Users, { as: 'parentZoom', foreignKey: 'childrenUserId', through: Zoom_meeting });

db.users = Users
db.roles = Roles

module.exports = db;
