module.exports = (sequelize, Sequelize) => {
    const Users_like = sequelize.define("users_like", {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
      },
      likes: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Users_like;
  };
  