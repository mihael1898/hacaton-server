module.exports = app => {
  const authUtil = require("../utils/auth-util");
  const passport = require('passport');
  const user = require("../controllers/user.controller.js");
  var Recaptcha = require('express-recaptcha').RecaptchaV3;
  var recaptcha = new Recaptcha('6LdHNPQUAAAAAI_xW8i6OC60x0LHgofVJrWZpZ83', '6LdHNPQUAAAAAI_xW8i6OC60x0LHgofVJrWZpZ83');
  var router = require("express").Router();
  // Authorize user
  router.post("/login", recaptcha.middleware.verify, authUtil.authenticate('local', {session: false}), (req, res) => {
    res.send({}) ;
  });

  // router.post("/register", recaptcha.middleware.verify, user.create);

  // router.get('/auth/google',addSocketIdtoSession,
  //   passport.authenticate('google', {session: false, scope: ['profile', 'email'] }));

  // router.get('/auth/google/callback', authUtil.authenticate('google', {session: false}), (req, res) => {
  //   res.redirect('http://localhost:8080')
  // });
  // router.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

  // router.get('/auth/google/callback', authUtil.authenticate('google', {session: true}), (req, res) => {
  //   res.redirect('http://localhost:8080')
  // });



  // router.post("/password", recaptcha.middleware.verify, user.getPassword);
  
  app.use('/api', router);
};
  