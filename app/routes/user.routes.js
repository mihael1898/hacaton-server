module.exports = app => {
  const user = require("../controllers/user.controller.js");
  const passport = require('passport');
  var router = require("express").Router();

  var multer = require("multer");

  var fileFilterImage = (req, file, cb) => {
    const allowedTypes = ["image/jpeg", "image/jpg", "image/png"];
    if (!allowedTypes.includes(file.mimetype)) {
        const error = new Error("Incorrect file");
        error.code = "INCORRECT_FILETYPE";
        return cb(error, false)
    }
    cb(null, true);
  }

  var storageImage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/usersImage')
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + '-' + file.originalname.replace(' ',''))
    }
  })

  var uploadImage = multer({
    storage : storageImage,
    fileFilter: fileFilterImage,
  });

  // Create a new Tutorial
  router.post("/", user.create);

  // Retrieve all user
  router.get("/all", passport.authenticate('jwt', {session: false}), user.findAll);

  // Retrieve a single Tutorial with id
  router.get("/:id", passport.authenticate('jwt', {session: false}), user.findOne);

  // Update a Tutorial with id
  router.put("/:id", passport.authenticate('jwt', {session: false}), uploadImage.single('file'), user.update);

  // Update a Tutorial with id
  router.put("/:id/addLike", passport.authenticate('jwt', {session: false}), user.updateLike);

  // Update a Tutorial with id
  router.put("/:id/addZoom", passport.authenticate('jwt', {session: false}), user.updateZoom);

  // Authorize user
  // router.put("/authorize", ()=>{
  //   passport.use(new LocalStrategy());
  // });

  // Delete a Tutorial with id
  // router.delete("/:id", user.delete);

  // Create a new Tutorial
  // router.delete("/", user.deleteAll);

  app.use('/api/users', router);
};
