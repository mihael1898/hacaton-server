module.exports = app => {
  const role = require("../controllers/role.controller.js");
  const passport = require('passport');
  var router = require("express").Router();

  // Create a new Tutorial
  router.post("/", passport.authenticate('jwt-admin', {session: false}), role.create);

  // Retrieve all role
  router.get("/", passport.authenticate('jwt-admin', {session: false}), role.findAll);

  // Retrieve a single Tutorial with id
  router.get("/:id", passport.authenticate('jwt-admin', {session: false}), role.findOne);

  // Update a Tutorial with id
  router.put("/:id", passport.authenticate('jwt-admin', {session: false}), role.update);

  // Delete a Tutorial with id
  router.delete("/:id", passport.authenticate('jwt-admin', {session: false}), role.delete);

  // Create a new Tutorial
  router.delete("/", passport.authenticate('jwt-admin', {session: false}), role.deleteAll);

  app.use('/api/roles', router);
};
