const db = require("../models");
const Roles = db.roles;
const Op = db.Sequelize.Op;

// Create and Save a new Roles
exports.create = (req, res) => {
  // Validate request
  if (!req.body.name) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Roles
  const roles = {
    name: req.body.name,
  };

  // Save Roles in the database
  return db.sequelize.transaction(t => {
    return Roles.create(roles, { transaction: t })
  }).then(data => {
      return res.send(data);
    })
    .catch(err => {
      return res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Roles."
      });
    });
};

// Retrieve all Roless from the database.
exports.findAll = (req, res) => {
  return db.sequelize.transaction(t => {
    return Roles.findAll({ transaction: t })
  }).then(data => {
      return res.send(data);
    })
    .catch(err => {
      return res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Roless."
      });
    });
};

// Find a single Roles with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  return db.sequelize.transaction(t => {
    return Roles.findByPk(id, { transaction: t })
  }).then(data => {
      return res.send(data);
    })
    .catch(err => {
      return res.status(500).send({
        message: "Error retrieving Roles with id=" + id
      });
    });
};

// Update a Roles by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  return db.sequelize.transaction(t => {
    return Roles.update(req.body, {
      where: { id: id },
      transaction: t
    })
  }).then(num => {
      if (num == 1) {
        return res.send({
          message: "Roles was updated successfully."
        });
      } else {
        return res.send({
          message: `Cannot update Roles with id=${id}. Maybe Roles was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      return res.status(500).send({
        message: "Error updating Roles with id=" + id
      });
    });
};

// Delete a Roles with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;
  return db.sequelize.transaction(t => {
    return Roles.destroy({
      where: { id: id },
      transaction: t
    })
  }).then(num => {
      if (num == 1) {
        return res.send({
          message: "Roles was deleted successfully!"
        });
      } else {
        return res.send({
          message: `Cannot delete Roles with id=${id}. Maybe Roles was not found!`
        });
      }
    })
    .catch(err => {
      return res.status(500).send({
        message: "Could not delete Roles with id=" + id
      });
    });
};

// Delete all Roless from the database.
exports.deleteAll = (req, res) => {
  return db.sequelize.transaction(t => {
    return Roles.destroy({
      where: {},
      truncate: false,
      transaction: t
    })
  }).then(nums => {
      return res.send({ message: `${nums} Roless were deleted successfully!` });
    })
    .catch(err => {
      return res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Roless."
      });
    });
};

// find all published Roles
// exports.findAllPublished = (req, res) => {
//   Roles.findAll({ where: { published: true } })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving Roless."
//       });
//     });
// };
