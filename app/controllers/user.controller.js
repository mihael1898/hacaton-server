const db = require("../models");
const authUtil = require("../utils/auth-util");
const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");
const Users = db.users;
const Roles = db.roles;
const Op = db.Sequelize.Op;
const fetch = require('node-fetch');

// Create and Save a new Users
exports.create = (req, res) => {
  // Validate request
  if (!req.body.pineToolId && !req.body.password) {
    return res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  Users.findOne({where: {pineToolId: req.body.pineToolId}}).then(user => {
    if(user) {
      return res.status(400).send({
        message: "User already exists!"
      });
    }
    else {
      // Create a Users
      const users = {
        pineToolId: req.body.pineToolId,
        password: authUtil.generateHash(req.body.password),
        name: req.body.name,
        surname: req.body.surname,
        position: req.body.position,
        about: req.body.about,
        user_role: 1
      };
      // Save Users in the database
      return db.sequelize.transaction(t => {
        return Users.create(users, {
          include: [ { model: Roles, as: 'role'} ],
          transaction: t
        })
      }).then(user => {
          const token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // will expire in 1 day
            // exp: Math.floor(Date.now() / 1000) + (60 * 5), // will expire in 5 min
            data: {id: user.dataValues.id}
          }, authUtil.getJwtSecret());
          return res.send({user: user.dataValues, token: token})
          // authUtil.authenticate('local', {session: false})
        })
        .catch(err => {
          return res.status(500).send({
            message:
              err.message || "Some error occurred while creating the Users."
          });
        });
    }
  })
  .catch(err => {
    return res.status(500).send({
      message:
        err.message || "Some error occurred while creating the Users."
    });
  });
};

//Retrieve all Userss from the database.
exports.findAll = (req, res) => {
  return db.sequelize.transaction(t => {
    return Users.findAll({
      transaction: t,
      include: [ { model: Roles, as: 'role'}, { model: Users, as: 'children'} ]
    })
  }).then(data => {
      return res.send(data);
    })
    .catch(err => {
      return res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving Userss."
      });
    });
};

// Find a single Users with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  if (!req.user || !(req.user.id === +id || req.user.user_role === 2)) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }
  return db.sequelize.transaction(t => {
    return Users.findByPk(id, {
      transaction: t,
      include: [ { model: Roles, as: 'role'}, { model: Users, as: 'children'}, { model: Users, as: 'parent'}, { model: Users, as: 'childrenZoom'}, { model: Users, as: 'parentZoom'} ]
    })
  }).then(data => {
      if(data) {
        return res.send(data);
      }
      else {
        return res.status(500).send({
          message: "Error retrieving Users with id=" + id
        });
      }
    })
    .catch(err => {
      return res.status(500).send({
        message: "Error retrieving Users with id=" + id
      });
    });
};

// // Find a single Users with an id
// exports.getPassword = (req, res) => {
//   if (req.recaptcha.data.score < 0.5) {
//     return res.status(400).send({
//       message: "Content can not be empty!"
//     });
//   }
//   var result = '';
//   var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//   var charactersLength = characters.length;
//   for ( var i = 0; i < 16; i++ ) {
//     result += characters.charAt(Math.floor(Math.random() * charactersLength));
//   }
//   console.log(result)
//   return db.sequelize.transaction(t => {
//     return Users.update({ password: authUtil.generateHash(result) }, {
//       where: {
//         email: req.body.email
//       },
//       transaction: t, 
//       include: [ { model: Roles, as: 'role'}, { model: Addresses, as: 'Addresses' }, { model: Orders, as: 'Orders', include: [{ model: Size_product, as: 'SizeProducts', include: [{ model: Products, include: [{ model: Photo, as: 'Photos' }]}, Sizes] }] }]
//     })
//   }).then(num => {
//       if (num == 1) {
//         var transporter = nodemailer.createTransport({
//           host: "smtp.gmail.com",
//           port: 465,
//           secure: true, // true for 465, false for other ports
//           auth: {
//             user: "testsendmailimei@gmail.com", // generated ethereal user
//             pass: "Qwertynum78" // generated ethereal password
//           }
//         });
//         var info = {
//           // from: '"Fred Foo 👻" <foo@example.com>', // sender address
//           from: "testsendmailimei@gmail.com", // sender address
//           to: req.body.email, // list of receivers
//           subject: "Сообщение!", // Subject line
//           text: result, // plain text body
//           // html: "<b>Hello world?</b>" // html body
//         };
//         transporter.sendMail(info, function(err) {
//           if (err) {
//             console.log(err)
//             return res.status(500).send('Something broke!')
//               // check if htmlstream is still open and close it to clean up
//           }
//           else{
//             return res.status(200).send('Ok');
//           }
//         });
//       } else {
//         return res.send({
//           message: `Cannot update Users with id=${id}. Maybe Users was not found or req.body is empty!`
//         });
//       }
//     })
//     .catch(err => {
//       return res.status(500).send({
//         message: "Error updating Users with id=" + id
//       });
//     });
// };

// Update a Users by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  if (req.file) {
    req.body.url_img = req.protocol + '://' + req.get('host') + req.file.path.substring(6)
    req.body.url_img_for_delete = req.file.path
  }
  if (req.body.password) {
    req.body.password = authUtil.generateHash(req.body.password)
  }
  return db.sequelize.transaction(t => {
    return Users.update(req.body, {
      where: { id: id },
      transaction: t, 
      individualHooks: true
    })
  }).then(num => {
    if (req.file) {
      return res.send({
        url_img: req.protocol + '://' + req.get('host') + req.file.path.substring(6)
      });
    } else if (num == 1) {
        return res.send({
          message: "Users was updated successfully."
        });
      } else {
        return res.send({
          message: `Cannot update Users with id=${id}. Maybe Users was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      return res.status(500).send({
        message: "Error updating Users with id=" + id
      });
    });
};


// Update a Users by the id in the request
exports.updateLike = (req, res) => {
  const id = req.params.id;
  return db.sequelize.transaction(t => {
    return Users.findOne({
      where: { id: id },
      transaction: t, 
    }).then(user => {
      return user.addChildren(req.body.id, { through: { likes: req.body.likes}, transaction: t })
      .then(data => {
        return res.send(data);
      })
      .catch(err => {
        return res.status(500).send({
          message: "Error retrieving Users with id=" + id
        });
      });
    })
    .catch(err => {
      return res.status(500).send({
        message: "Error updating Users with id=" + id
      });
    });
  })
};

// Update a Users by the id in the request
exports.updateZoom = (req, res) => {
  let token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6InBqV0xEb0l4UzdLUkQ2X3FiVmZHd2ciLCJleHAiOjE1OTIwMjQ0MTEsImlhdCI6MTU5MTQxOTYxM30.UnIAT2322K0vrcHTQgPfevnr8fJEHxyzafsq78gu7RA"
  fetch('https://api.zoom.us/v2/users/ACsxGqa8St6IdHFzSL0W5A/meetings', {
      method: 'post',
      body:    JSON.stringify({topic: 'Meeting title #1', type: 1, agenda: "За три минуты опишите: область интересов, основные проекты, сильные стороны, потребности, запросы"}),
      headers: { 'Content-Type': 'application/json',
      'authorization': token,
     },
  })
  .then(res => res.json())
  .then(json => {
    const id = req.params.id;
    return db.sequelize.transaction(t => {
      return Users.findOne({
        where: { id: id },
        transaction: t, 
      }).then(user => {
        return user.addChildrenZoom(req.body.id, { through: { idZoom: json.id, startUrl: json.start_url, joinUrl: json.join_url, password: json.password }, transaction: t })
        .then(data => {
          return res.send(data);
        })
        .catch(err => {
          return res.status(500).send({
            message: "Error retrieving Users with id=" + id
          });
        });
      })
      .catch(err => {
        return res.status(500).send({
          message: "Error updating Users with id=" + id
        });
      });
    })

  });
};

// exports.verifyPassword(){
  
// }

// Delete a Users with the specified id in the request
// exports.delete = (req, res) => {
//   const id = req.params.id;

//   Users.destroy({
//     where: { id: id }, include: [ { model: Roles, as: 'role'}, { model: Addresses, as: 'Addresses' }]
//   })
//     .then(num => {
//       if (num == 1) {
//         res.send({
//           message: "Users was deleted successfully!"
//         });
//       } else {
//         res.send({
//           message: `Cannot delete Users with id=${id}. Maybe Users was not found!`
//         });
//       }
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: "Could not delete Users with id=" + id
//       });
//     });
// };

// Delete all Userss from the database.
// exports.deleteAll = (req, res) => {
//   Users.destroy({
//     where: {},
//     truncate: false,
//     include: [ { model: Roles, as: 'role'}, { model: Addresses, as: 'Addresses' }]
//   })
//     .then(nums => {
//       res.send({ message: `${nums} Userss were deleted successfully!` });
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while removing all Userss."
//       });
//     });
// };

// find all published Users
// exports.findAllPublished = (req, res) => {
//   Users.findAll({ where: { published: true } })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message:
//           err.message || "Some error occurred while retrieving Userss."
//       });
//     });
// };
