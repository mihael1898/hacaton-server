const bcrypt = require("bcryptjs/index");
const jwt = require('jsonwebtoken');
const passport = require('passport');
// const Cookies = require('cookies')

var getJwtSecret = function () {
  return 'fe1a1915a379f3be5394b64d14794932';
}
exports.getJwtSecret = getJwtSecret

var getBcryptSecret = function () {
  return '$2$06$Irkut6kFore5er7ins.L0l';
}
exports.getBcryptSecret = getBcryptSecret

var generateHash = function (password) {
  // return bcrypt.hashSync(password, getBcryptSecret());
  return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}
exports.generateHash = generateHash 

exports.validatePassword = (userPassword, hashedPassword)=> {
  // return generateHash(userPassword) === hashedPassword;
  return bcrypt.compareSync(userPassword, hashedPassword);
}

exports.generateHashAsync=(password) =>{
  return bcrypt.hash(password, getBcryptSecret());
}

exports.authenticate=(strategy, options)=> {
  return function (req, res, next) {
    if (req.recaptcha && req.recaptcha.data.score < 0.5) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
    passport.authenticate(strategy, options, (error, user , info) => {
      if (error) {
        return next(error);
      }
      if (!user) {
        return next(new TranslatableError('unauthorised', HTTPStatus.UNAUTHORIZED));
      }
      if (options.session) {
        return req.login(user, (err) => {
          if (err) {
            res.send(err);
          }
          const token = jwt.sign({
            exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // will expire in 1 day
            // exp: Math.floor(Date.now() / 1000) + (60 * 5), // will expire in 5 min
            data: {id: user.id}
          }, getJwtSecret());
          req.session.token = token
          return next()
        });
      }
      // if (strategy === 'google') {
      //   const io = req.app.get('io')
      //   const token = jwt.sign({
      //     exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // will expire in 1 day
      //     // exp: Math.floor(Date.now() / 1000) + (60 * 5), // will expire in 5 min
      //     data: {id: user.dataValues.id}
      //   }, getJwtSecret());
      //   io.to(req.session.socketId).emit('googleAuth', {user: user.dataValues, token: token})
      //   next()
      // }
      
      const token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24), // will expire in 1 day
        // exp: Math.floor(Date.now() / 1000) + (60 * 5), // will expire in 5 min
        data: {id: user.id}
      }, getJwtSecret());
      // var cookies = new Cookies(req, res)
      // console.log('sdfdsfs')
      // cookies.set('_user', encodeURIComponent(user.id), { maxAge: 60 * 5, httpOnly: true, sameSite: 'lax'  })
      // console.log('sdfdsfsqqqqq')
      // cookies.set('_bearer_token', encodeURIComponent(token), { maxAge: 60 * 5, httpOnly: true, sameSite: 'lax'  })
      // res.cookie('_user', encodeURIComponent(user.id), { maxAge: 60 * 5, sameSite: 'lax' });
      // res.cookie('_bearer_token', encodeURIComponent(token), { maxAge: 60 * 5, sameSite: 'lax' });
        return res.json({user: user, token: token});
    })(req, res, next);
  };
}


// export {
//     getJwtSecret, getBcryptSecret, generateHash, validatePassword, generateHashAsync, generateHashAsync, authenticate
// }