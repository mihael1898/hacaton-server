const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy   = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;

const db = require("./app/models");
const authUtil = require("./app/utils/auth-util")
const User = db.users;
const Op = db.Sequelize.Op;

passport.use('local', new LocalStrategy({
      usernameField: 'pineToolId',
      passwordField: 'password'
  }, 
  function (pineToolId, password, done) {
    //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
    return db.sequelize.transaction(t => {
      return User.findOne({
        transaction: t,
        where: {
          pineToolId: pineToolId
        },
        include: [ { model: db.roles, as: 'role'} ]
      })
    }).then(user => {
      // if (user && authUtil.validatePassword(password, user.password)) {
      if (user && authUtil.validatePassword(password, user.dataValues.password)) {
        return done(null, user.dataValues, {message: 'Logged In Successfully'});
      }
      return done(null, false, {message: 'Incorrect email or password.'});
      
    })
    .catch(err => done(err));
  }
));

passport.use('jwt', new JWTStrategy({
    jwtFromRequest: ExtractJWT.fromHeader('authorization'),
    secretOrKey: authUtil.getJwtSecret()
  },
  function (jwtPayload, cb) {
    console.log(jwtPayload)
    //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
    return db.sequelize.transaction(t => {
      return User.findByPk(jwtPayload.data.id, {
        transaction: t,
        include: [ { model: db.roles, as: 'role'}]
      })
    }).then(user => {
            return cb(null, user.dataValues);
        })
        .catch(err => {
          console.log(err)
            return cb(err);
        });
  }
));

passport.use('jwt-admin', new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromHeader('authorization'),
  secretOrKey: authUtil.getJwtSecret()
  },
  function (jwtPayload, cb) {
    //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
    return db.sequelize.transaction(t => {
      return User.findOne({
        transaction: t,
        where: {
          id: jwtPayload.data.id,
          user_role: 2 
        },
        include: [ { model: db.roles, as: 'role'}]
      })
    }).then(user => {
          if (user){
            return cb(null, user.dataValues);
          }
          else {
            return cb(null, false);
          } 
        })
        .catch(err => {
            return cb(err);
        });
  }
));

passport.use('jwt-manager', new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromHeader('authorization'),
  secretOrKey: authUtil.getJwtSecret()
  },
  function (jwtPayload, cb) {
    //find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
    return db.sequelize.transaction(t => {
      return User.findOne({
        transaction: t,
        where: { 
          id: jwtPayload.data.id,
          user_role: {
            [Op.or]: [2, 3]
          } 
        },
        include: [ { model: db.roles, as: 'role'}]
      })
    }).then(user => {
          if (user){
            return cb(null, user.dataValues);
          }
          else {
            return cb(null, false);
          } 
        })
        .catch(err => {
            return cb(err);
        });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  return db.sequelize.transaction(t => {
    return User.findByPk(id, {
      transaction: t,
      include: [ { model: db.roles, as: 'role'}]
    })
  }).then(user => {
      return done(null, user.dataValues);
    })
    .catch(err => {
        return done(err);
    });
});